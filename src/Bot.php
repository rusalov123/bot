<?php
namespace CmmntBot;


class Bot{
	public $url;
	private $token='';
	function __construct($token,$url='http://app.cmmnt.net/api/bot/'){
		$this->url=$url;
		$this->token=$token;
	}
	function sendMessage($sign,$to,$body,$element=false){
			$p=array(
									'to' => $to,
									'body' =>$body,
									'sign' =>$sign,
									
				);
				if(!empty($element)){
					$p['element']=json_encode($element);
				}
								
			$opts = array('http' =>
				array(
					'method'  => 'POST',
					'header'  => 'Content-Type: application/x-www-form-urlencoded',
					'content' => http_build_query(
								$p
							)
				)
			);
			$context  = stream_context_create($opts);
			$body=file_get_contents($this->url .'messages?token='.$this->token, false, $context);
			return json_decode($body,true);
	}
	function setWebhook($url){
			$opts = array('http' =>
				array(
					'method'  => 'POST',
					'header'  => 'Content-Type: application/x-www-form-urlencoded',
					'content' => http_build_query(
								array(
									'url' => $url,
								)
							)
				)
			);
			$context  = stream_context_create($opts);
			$body=file_get_contents($this->url .'webhook?token='.$this->token, false, $context);
			return json_decode($body,true);
	}
	function deleteWebhook($url){
			$opts = array('http' =>
				array(
					'method'  => 'DELETE',
					'header'  => 'Content-Type: application/x-www-form-urlencoded',
					'content' => http_build_query(
								array(
									'url' => $url,
								)
							)
				)
			);
			$context  = stream_context_create($opts);
			$body=file_get_contents($this->url .'webhook?token='.$this->token, false, $context);
			return json_decode($body,true);
	}
	function updateCommands($command){
			$opts = array('http' =>
				array(
					'method'  => 'POST',
					'header'  => 'Content-Type: application/x-www-form-urlencoded',
					'content' => http_build_query(
								array(
									'command' => $command,
								)
							)
				)
			);
			$context  = stream_context_create($opts);
			$body=file_get_contents($this->url .'commands?token='.$this->token, false, $context);
			return json_decode($body,true);
	}
	function getMessages($before=0,$limit=50){
			$q=array();
			if($before)$q['before']=$before;
			if($limit)$q['limit']=$limit;
			 
			$opts = array('http' =>
				array(
					'method'  => 'GET',
					'header'  => 'Content-Type: application/x-www-form-urlencoded',
					'content' => http_build_query(
								$q
							)
				)
			);
			$context  = stream_context_create($opts);
			$body=file_get_contents($this->url .'messages?token='.$this->token, false, $context);
			return json_decode($body,true);
	}
	function getInfo($jid){
			$opts = array('http' =>
				array(
					'method'  => 'GET',
					'header'  => 'Content-Type: application/x-www-form-urlencoded',
					'content' => http_build_query(
								array(
									'jid'=>$jid
								)
							)
				)
			);
			$context  = stream_context_create($opts);
			$body=file_get_contents($this->url .'?token='.$this->token, false, $context);
			return json_decode($body,true);
	}
 	 	function createBot($id,$name){
			$opts = array('http' =>
				array(
					'method'  => 'PUT',
					'header'  => 'Content-Type: application/x-www-form-urlencoded',
					'content' => http_build_query(
								array(
									'id'=>$id,
									'name'=>$name,
								)
							)
				)
			);
			$context  = stream_context_create($opts);
			$body=file_get_contents($this->url .'?token='.$this->token, false, $context);
			return json_decode($body,true);
	}
	  
}